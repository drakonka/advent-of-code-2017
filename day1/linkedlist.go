package main

import (
	"container/list"
	"strconv"
)


func prepLinkedList() *list.List{
	llist := list.New()
	for _, v := range input {
		i, _ := strconv.Atoi(string(v))
		llist.PushBack(i)
	}
	return llist
}

func runPartOneLL() int {
	llist := prepLinkedList()
	var sum int
	for e := llist.Front(); e != nil; e = e.Next() {
		val := e.Value.(int)
		var compVal int
		if e.Prev() != nil {
			compVal = e.Prev().Value.(int)
		} else {
			compVal = llist.Front().Value.(int)
		}
		if val == compVal {
			sum += compVal
		}
	}
	return sum
}

func runPartTwoLL() int {
	llist := prepLinkedList()
	var sum int
	steps := llist.Len() / 2
	for e := llist.Front(); e != nil; e = e.Next() {
		val := e.Value.(int)
		comparison := e
		for i := 0; i < steps; i++ {
			if comparison.Next() != nil {
				comparison = comparison.Next()
			} else {
				comparison = llist.Front()
			}
		}

		if val == comparison.Value.(int) {
			sum += val
		}
	}
	return sum
}
