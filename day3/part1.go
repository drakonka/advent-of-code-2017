package main

import "math"

func runPartOne() int {
	// bottom right diagonal seems to be sequence of odd squares...
	origin := coords{0,0}
	var steps, dist int
	brCorner := 1.0
	for brCorner < input {
		nextOdd := getNextOdd(brCorner)
		brCorner = nextOdd * nextOdd
		steps++
	}
	brCornerCoords := coords{steps, -steps}
	sideSize := math.Sqrt(brCorner)
	distFromCorner := int(brCorner - input)
	marker := traverseBack(brCornerCoords, int(sideSize), distFromCorner)
	dist = int(math.Abs(float64(marker.x - origin.x)) + math.Abs(float64(marker.y - origin.y)))
	return dist
}

func traverseBack(brCorner coords, sideSize int, steps int) coords {
	blCorner, tlCorner, trCorner := getCornerCoords(brCorner, sideSize)

	direction := left
	c := brCorner
	for i := 0; i < steps; i++ {
		direction = getClockwiseDirection(c, direction, blCorner, tlCorner, trCorner)
		c.x += direction.x
		c.y += direction.y
	}
	return c
}
func getClockwiseDirection(c coords, direction coords,  blCorner coords, tlCorner coords, trCorner coords) coords {
	if c == blCorner {
		direction = up
	} else if c == tlCorner {
		direction = right
	} else if c == trCorner {
		direction = down
	}
	return direction
}

func getCornerCoords(brCorner coords, sideSize int) (coords, coords, coords) {
	blCorner := coords{
		x: brCorner.x - sideSize,
		y: brCorner.y,
	}
	tlCorner := coords{
		x: blCorner.x,
		y: blCorner.y + sideSize,
	}
	trCorner := coords{
		x: tlCorner.x + sideSize,
		y: tlCorner.y,
	}
	return blCorner, tlCorner, trCorner
}


func getNextOdd(n float64) float64 {
	var nextOdd float64
	if n > 1 {
		nextOdd = math.Sqrt(n) + 2
	} else {
		nextOdd = 3
	}
	return nextOdd
}
