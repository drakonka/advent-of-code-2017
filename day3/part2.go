package main

import "container/list"

func runPartTwo() int {
	cells := map[coords]int{}
	directions := new(list.List)
	directions.PushBack(down)
	directions.PushBack(right)
	directions.PushBack(up)
	directions.PushBack(left)

	marker := coords{0, 0}
	cells[marker] = 1

	direction := directions.Front()
	for cells[marker] <= input {
		var nextdir *list.Element
		if direction.Next() != nil {
			nextdir = direction.Next()
		} else {
			nextdir = directions.Front()
		}
		futurecoords := coords{
			x: marker.x + nextdir.Value.(coords).x,
			y: marker.y + nextdir.Value.(coords).y}
		if _, ok := cells[futurecoords]; ok {
			nextdir := direction
			futurecoords = coords{
				x: marker.x + nextdir.Value.(coords).x,
				y: marker.y + nextdir.Value.(coords).y}
		} else {
			direction = nextdir
		}
		marker = futurecoords
		val := getCellVal(marker, cells)
		cells[marker] = val
	}
	return cells[marker]
}

func getCellVal(marker coords, grid map[coords]int) int {
	pn := []coords{
		{x: marker.x, y: marker.y + 1},
		{x: marker.x, y: marker.y - 1},
		{x: marker.x + 1, y: marker.y},
		{x: marker.x + 1, y: marker.y + 1},
		{x: marker.x + 1, y: marker.y - 1},
		{x: marker.x - 1, y: marker.y},
		{x: marker.x - 1, y: marker.y - 1},
		{x: marker.x - 1, y: marker.y + 1},
	}
	var cellVal int
	for _, c := range pn {
		if val, ok := grid[c]; ok {
			cellVal += val
		}
	}
	return cellVal

}
