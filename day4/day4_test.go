package main

import (
	"testing"
	"os"
	"gitlab.com/drakonka/adventofcode2017/util"
)

func TestMain(m *testing.M) {
	ret := m.Run()
	if ret == 0 {
		util.CreateReportHeaders(4)
		p1res := testing.Benchmark(BenchmarkD4P1)
		util.RecordBenchRes(4, 1, "", p1res)
		p2res := testing.Benchmark(BenchmarkD4P2)
		util.RecordBenchRes(4, 2, "", p2res)
	}
	os.Exit(ret)
}

func TestD4P1Answer(t *testing.T) {
	want := 386
	a := runPartOne()
	if a != want {
		t.Errorf("Incorrect Answer! Expected %d, got %d", want, a )
	}
}

func BenchmarkD4P1(b *testing.B) {
	for n := 0; n < b.N; n++ {
		runPartOne()
	}
}

func TestD4P2Answer(t *testing.T) {
	want := 208
	a := runPartTwo()
	if a != want {
		t.Errorf("Incorrect Answer! Expected %d, got %d", want, a )
	}
}

func BenchmarkD4P2(b *testing.B) {
	for n := 0; n < b.N; n++ {
		runPartTwo()
	}
}