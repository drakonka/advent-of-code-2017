package main

import (
	"os"
	"runtime"
	"fmt"
	"bufio"
	"strconv"
	"gitlab.com/drakonka/adventofcode2017/util"
)

func main() {
	fmt.Println("----------------------------------------")
	fmt.Println("DAY 5")
	fmt.Println("----------------------------------------")

	fmt.Printf("\nPart 1 res: %d", runPartOne())
	fmt.Printf("\nPart 2 res: %d", runPartTwo())


	fmt.Println()
	fmt.Println("----------------------------------------")
	fmt.Println("END DAY 5")
	fmt.Println("----------------------------------------")
}

func runPartOne() int {
	inputFile := getInputFile()
	defer inputFile.Close()
	scanner := bufio.NewScanner(inputFile)

	instructions := []int{}
	for scanner.Scan() {
		line := scanner.Text()
		i, _ := strconv.Atoi(line)
		instructions = append(instructions, i)
	}

	i := 0
	steps := 0
	for i >= 0 && i < len(instructions) {
		val := instructions[i]
		instructions[i]++
		i += val
		steps++
	}
	return steps
}

func runPartTwo() int {
	inputFile := getInputFile()
	defer inputFile.Close()
	scanner := bufio.NewScanner(inputFile)
	instructions := []int{}
	for scanner.Scan() {
		line := scanner.Text()
		i, _ := strconv.Atoi(line)
		instructions = append(instructions, i)
	}

	i := 0
	steps := 0
	for i >= 0 && i < len(instructions) {
		val := instructions[i]
		if val >= 3 {
			instructions[i]--
		} else {
			instructions[i]++
		}
		i += val
		steps++
	}
	return steps
}

func getInputFile() *os.File {
	_, filename, _, ok := runtime.Caller(0)
	if !ok {
		return nil
	}
	return util.GetInputFile(filename)
}
