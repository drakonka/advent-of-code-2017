package main

import (
	"testing"
	"os"
	"gitlab.com/drakonka/adventofcode2017/util"
)

func TestMain(m *testing.M) {
	ret := m.Run()
	if ret == 0 {
		util.CreateReportHeaders(5)
		p1res := testing.Benchmark(BenchmarkD5P1)
		util.RecordBenchRes(5, 1, "", p1res)
		p2res := testing.Benchmark(BenchmarkD5P2)
		util.RecordBenchRes(5, 2, "", p2res)
	}
	os.Exit(ret)
}

func TestD5P1Answer(t *testing.T) {
	want := 354121
	a := runPartOne()
	if a != want {
		t.Errorf("Incorrect Answer! Expected %d, got %d", want, a )
	}
}

func BenchmarkD5P1(b *testing.B) {
	for n := 0; n < b.N; n++ {
		runPartOne()
	}
}

func TestD5P2Answer(t *testing.T) {
	want := 27283023
	a := runPartTwo()
	if a != want {
		t.Errorf("Incorrect Answer! Expected %d, got %d", want, a )
	}
}

func BenchmarkD5P2(b *testing.B) {
	for n := 0; n < b.N; n++ {
		runPartTwo()
	}
}