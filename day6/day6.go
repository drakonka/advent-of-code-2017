package main

import (
	"os"
	"runtime"
	"gitlab.com/drakonka/adventofcode2017/util"
	"strconv"
	"fmt"
	"bufio"
	"strings"
)

func main() {
	fmt.Println("----------------------------------------")
	fmt.Println("DAY 6")
	fmt.Println("----------------------------------------")

	p1, p2 := runPuzzle()
	fmt.Printf("\nPart 1 res: %d", p1)
	fmt.Printf("\nPart 2 res: %d", p2)


	fmt.Println()
	fmt.Println("----------------------------------------")
	fmt.Println("END DAY 6")
	fmt.Println("----------------------------------------")
}

func getInputFile() *os.File {
	_, filename, _, ok := runtime.Caller(0)
	if !ok {
		return nil
	}
	return util.GetInputFile(filename)
}


func runPuzzle() (int, int) {
	inputFile := getInputFile()
	defer inputFile.Close()
	cycles := 0
	var banks []int
	scanner := bufio.NewScanner(inputFile)
	var highestIdx, highestVal int
	for scanner.Scan() {
		line := scanner.Text()
		s := strings.Split(line, "\t")
		for idx, c := range s {
			i, _ := strconv.Atoi(c)
			banks = append(banks, i)
			if i > highestVal {
				highestVal = i
				highestIdx = idx
			}
		}
	}

	var history []string
	var lastBank = getStr(banks)
	for !sliceContains(history, lastBank) {
		history = append(history, lastBank)
		idx := highestIdx
		val := banks[idx]
		banks[idx] = 0
		for val > 0 {
			if idx + 1 < len(banks) {
				idx++
				banks[idx]++
			} else {
				idx = 0
				banks[idx]++
			}
			val--
		}
		cycles++
		lastBank = getStr(banks)
		highestIdx = getHighestIdx(banks)
	}
	var his2 []string
	var c2 int
	for !sliceContains(his2, lastBank) {
		his2 = append(his2, lastBank)
		idx := highestIdx
		val := banks[idx]
		banks[idx] = 0
		for val > 0 {
			if idx + 1 < len(banks) {
				idx++
				banks[idx]++
			} else {
				idx = 0
				banks[idx]++
			}
			val--
		}
		c2++
		lastBank = getStr(banks)
		highestIdx = getHighestIdx(banks)
	}

	return cycles, c2
}

func getHighestIdx(b []int) int {
	var hival, hiidx int
	for i, val := range b {
		if val > hival {
			hival = val
			hiidx = i
		}
	}
	return hiidx
}

func getStr(banks []int) string {
	var s string
	for _, i := range banks {
		ic := strconv.Itoa(i)
		s += ic + ","
	}
	return s
}

func sliceContains(s []string, b string) bool {
	for _, c := range s {
		if c == b {
			return true
		}
	}
	return false
}