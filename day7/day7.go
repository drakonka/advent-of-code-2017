package main

import (
	"fmt"
	"os"
	"runtime"
	"gitlab.com/drakonka/adventofcode2017/util"
	"bufio"
	"regexp"
	"strconv"
	"strings"
)

func main() {
	fmt.Println("----------------------------------------")
	fmt.Println("DAY 7")
	fmt.Println("----------------------------------------")

	rootName, balancedWeight := runP1P2()
	fmt.Printf("\nPart one: %s, Part 2: %d", rootName, balancedWeight)

	fmt.Println()
	fmt.Println("----------------------------------------")
	fmt.Println("END DAY 7")
	fmt.Println("----------------------------------------")
}

func getInputFile() *os.File {
	_, filename, _, ok := runtime.Caller(0)
	if !ok {
		return nil
	}
	return util.GetInputFile(filename)
}

func runP1P2() (string, int) {
	inputFile := getInputFile()
	defer inputFile.Close()
	var branches []*Branch
	var rootBranch *Branch

	scanner := bufio.NewScanner(inputFile)
	for scanner.Scan() {
		line := scanner.Text()
		branch := buildBranch(line)
		branches = append(branches, branch)
	}

	for _, b := range branches {
		b.findParent(branches)
		b.findBranches(branches)
		if b.parent == nil || b.parent.name == "" {
			rootBranch = b
		}
	}

	rootBranch.getStackWeights()
	badBranch := findBadStack(rootBranch)
	fixedWeight := badBranch.balanceWeight()
	return rootBranch.name, fixedWeight
}

func (b *Branch) balanceWeight() int {
	var newWeight int
	if b.parent != nil {
		for _, c := range b.parent.branches {
			if c != b {
				stackWeightDiff := c.stackweight - b.stackweight
				newWeight = b.weight + stackWeightDiff
				break
			}
		}
	}
	return newWeight
}

func findBadStack(b *Branch) *Branch {
	var badBranch *Branch
	if b.parent != nil {
		badBranch = b
	}
	stackWeights := map[int]int{}
	for _, c := range b.branches {
		stackWeights[c.stackweight]++
	}
	if len(stackWeights) > 1 && len(b.branches) > 0 {
		oddWeightOut := b.branches[0].stackweight
		oddWeightOutCount := stackWeights[oddWeightOut]
		for k, v := range stackWeights {
			if v < oddWeightOutCount {
				oddWeightOut = k
				oddWeightOutCount = v
			}
		}
		for _, c := range b.branches {
			if c.stackweight == oddWeightOut {
				bBranch := findBadStack(c)
				if bBranch != nil {
					badBranch = bBranch
				}
			}
		}
	}
	return badBranch
}

func buildBranch(line string) *Branch {
	namere := regexp.MustCompile("^[^\\s]+")
	name := namere.FindString(line)
	weightre := regexp.MustCompile("(\\d)+")
	weight, _ := strconv.Atoi(weightre.FindString(line))
	branch := Branch{
		name: name,
		weight: weight,
		stackweight: weight,
		children: getChildren(line),
	}

	return &branch
}

func (b *Branch) findParent(branches []*Branch) {
	for _, br := range branches {
		for _, ch := range br.children {
			if ch == b.name {
				b.parent = br
				return
			}
		}
	}
}

func (b *Branch) findBranches(branches []*Branch) {
	childnames := b.children
	for _, c := range childnames {
		for _, br := range branches {
			if br.name == c {
				b.branches = append(b.branches, br)
			}
		}
	}
}

func (b *Branch) getStackWeights() int {
	weight := b.weight
	for _, c := range b.branches {
		weight += c.getStackWeights()
	}
	b.stackweight = weight
	return weight
}

func getChildren(line string) []string {
	children := strings.Split(line, ",")
	for i := 0; i < len(children); i++ {
		c := children[i]
		if strings.Contains(c, " ") {
			sl := strings.Split(c, " ")
			children[i] = sl[len(sl)-1]
		}
	}
	return children
}

type Branch struct {
	name string
	weight int
	parent *Branch
	children []string
	branches []*Branch
	stackweight int
}