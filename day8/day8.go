package main

import (
	"fmt"
	"os"
	"runtime"
	"gitlab.com/drakonka/adventofcode2017/util"
	"bufio"
	"strings"
	"strconv"
)

type register struct {
	name string
	val int
}
type instruction struct {
	r *register
	action string
	val int
	c *condition
}

type condition struct {
	r1, con string
	num int
}

func main() {
	fmt.Println("----------------------------------------")
	fmt.Println("DAY 8")
	fmt.Println("----------------------------------------")

	p1, p2 := runPuzzle()
	fmt.Printf("\nPart one: %d; Part two: %d", p1, p2)

	fmt.Println()
	fmt.Println("----------------------------------------")
	fmt.Println("END DAY 8")
	fmt.Println("----------------------------------------")
}

func getInputFile() *os.File {
	_, filename, _, ok := runtime.Caller(0)
	if !ok {
		return nil
	}
	return util.GetInputFile(filename)
}

var (
	registers = map[string]*register{}
)

func runPuzzle() (int, int) {
	inputFile := getInputFile()
	defer inputFile.Close()
	var highestVal int // for part 2
	scanner := bufio.NewScanner(inputFile)
	for scanner.Scan() {
		line := scanner.Text()
		i := buildInstruction(line)
		if i.c.evaluate() {
			i.evaluate()
			if i.r.val > highestVal {
				highestVal = i.r.val
			}
		}
	}

	hr := findHighestRegister()
	return hr.val, highestVal
}

func findHighestRegister() *register {
	var a *register
	for _, v := range registers {
		if a == nil {
			a = v
		} else {
			if v.val > a.val {
				a = v
			}
		}
	}
	return a
}

func buildInstruction(l string) *instruction {
	parts := strings.Split(l, " ")
	num, _ := strconv.Atoi(parts[6])
	con := condition {
		r1: parts[4],
		num: num,
		con: parts[5],
	}
	if _, ok := registers[parts[0]]; !ok {
		r := register {
			name: parts[0],
			val: 0,
		}
		registers[r.name] = &r
	}
	val, _ := strconv.Atoi(parts[2])
	i := instruction {
		r: registers[parts[0]],
		action: parts[1],
		c: &con,
		val: val,
	}
	return &i
}

func (i *instruction) evaluate() {
	if i.action == "inc" {
		i.r.val += i.val
	} else if i.action == "dec" {
		i.r.val -= i.val
	}
}

func (c *condition) evaluate() bool {
	if _, ok := registers[c.r1]; !ok {
		registers[c.r1] = &register{name: c.r1, val:0}
	}
	v1 := registers[c.r1].val
	v2 := c.num
	if c.con == "<" {
		return v1 < v2
	} else if c.con == "<=" {
		return v1 <= v2
	} else if c.con == ">" {
		return v1 > v2
	} else if c.con == ">=" {
		return v1 >= v2
	} else if c.con == "==" {
		return v1 == v2
	} else if c.con == "!=" {
		return v1 != v2
	}
	fmt.Println("Something went horribly wrong.")
	return false
}

