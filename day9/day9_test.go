package main

import (
	"testing"
	"os"
	"gitlab.com/drakonka/adventofcode2017/util"
)

func TestMain(m *testing.M) {
	ret := m.Run()
	if ret == 0 {
		util.CreateReportHeaders(9)
		res := testing.Benchmark(Benchmark)
		util.RecordBenchRes(9, 0, "", res)
	}
	os.Exit(ret)
}

func TestAnswer(t *testing.T) {
	want1 := 9251
	want2 := 4322
	a1, a2 := runPuzzle()
	if a1 != want1 {
		t.Errorf("Incorrect Answer to part 1! Expected %s, got %s", want1, a1)
	}
	if a2 != want2 {
		t.Errorf("Incorrect Answer to part 2! Expected %d, got %d", want2, a2)
	}
}

func Benchmark(b *testing.B) {
	for n := 0; n < b.N; n++ {
		runPuzzle()
	}
}