package util

import (
	"fmt"
	"path"
	"os"
	"io/ioutil"
	"time"
	"testing"
	"runtime"
	"path/filepath"
)

var runTimestamp int64

func CreateReportHeaders(day int) {
	dirpath := getReportsDir(day)
	_ = os.Mkdir(dirpath, os.ModePerm)

	runTimestamp = time.Now().Unix()

	filename := fmt.Sprintf("%v.txt", runTimestamp)
	fullpath := path.Join(dirpath, filename)
	contents := []byte("Day, Part, Duration (ns), Allocs, Alloc. Bytes, Ops, Label")
	mode := os.FileMode(os.ModePerm)
	err := ioutil.WriteFile(fullpath, contents, mode)
	if err != nil {
		fmt.Printf("ERROR creating report headers: %s", err)
	}
}

func RecordBenchRes(day, part int, label string, res testing.BenchmarkResult) {
	if label == "" {
		label = "None"
	}
	dirpath := getReportsDir(day)
	filename := fmt.Sprintf("%v.txt", runTimestamp)
	fullpath := path.Join(dirpath, filename)
	file, err := os.OpenFile(fullpath, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
	if err != nil {
		fmt.Printf("\nERROR opening file: %s", err)
		return
	}
	line := []byte(fmt.Sprintf("\n%d, %d, %d, %d, %d, %d, %s", day, part, res.NsPerOp(), res.AllocsPerOp(), res.AllocedBytesPerOp(), res.N, label))
	_, err = file.Write(line)
	if err != nil {
		fmt.Printf("\nERROR writing to file: %s", err)
	}
}

func getReportsDir(day int) string {
	_, b, _, _ := runtime.Caller(0)
	d1 := filepath.Dir(b)
	basepath := filepath.Dir(d1)
	subdir := fmt.Sprintf("day-%d", day)
	dirpath := path.Join(basepath, "reports", subdir)
	return dirpath
}
